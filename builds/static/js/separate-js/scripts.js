//'use strict';

$(function() {

    /*
    |--------------------------------------------------------------------------
    | Currency Switcher
    |--------------------------------------------------------------------------
    */

    window.onload = () => {
        let toggleBtn = document.querySelector('.currency-switcher__toggle');
        let currencyBtns = document.querySelectorAll('.currency-switcher__list .currency-switcher__btn');
        currencyBtns.forEach(element => {

            element.addEventListener('click', (e) => {

                let icon = element.querySelector('.currency-switcher__icon').src;
                let text = element.querySelector('.currency-switcher__text').innerHTML;

                toggleBtn.querySelector('.currency-switcher__icon').src = icon;
                toggleBtn.querySelector('.currency-switcher__text').innerHTML = text;

                currencyBtns.forEach(e => {
                    e.classList.remove('--active');
                });

                toggleBtn.dataset.currency = element.dataset.currency;
                element.classList.add('--active');
                toggleBtn.classList.remove('--active');
            });

        });

        toggleBtn.addEventListener('click', e => {
            toggleBtn.classList.toggle('--active');
        });
    };

    /*
    |--------------------------------------------------------------------------
    | AOS Library
    |--------------------------------------------------------------------------
    */

    AOS.init();

    /*
    |--------------------------------------------------------------------------
    | Servers Checkbox
    |--------------------------------------------------------------------------
    */

    $("#serverComponentsCheckbox").click(function() {
        var checked = $("input:checked").length;
        if (checked == 0) {
            $('#components .collapse').removeAttr("data-parent");
            $('#components .collapse').collapse('show');
        } else {
            $('#components .collapse').attr("data-parent","#accordion");
            $('#components .collapse').collapse('hide');
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Product Features Checkbox
    |--------------------------------------------------------------------------
    */

    $("#productFeaturesCheckbox").click(function() {
        var checked = $("input:checked").length;
        if (checked == 0) {
            $('#product-features .collapse').removeAttr("data-parent");
            $('#product-features .collapse').collapse('show');

            $('#product-features-mobile .collapse').removeAttr("data-parent");
            $('#product-features-mobile .collapse').collapse('show');
        } else {
            $('#product-features .collapse').attr("data-parent","#accordion");
            $('#product-features .collapse').collapse('hide');

            $('#product-features-mobile .collapse').attr("data-parent","#accordion");
            $('#product-features-mobile .collapse').collapse('hide');
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Sticky Kit
    |--------------------------------------------------------------------------
    */

    if(window.matchMedia('(min-width: 992px)').matches) {
        $(".js-sticky-menu").stick_in_parent({
            recalc_every: 1,
            offset_top: 38,
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Fixed Header
    |--------------------------------------------------------------------------
    */

    $(function() {
        if ($('.header__menu').length > 0) {
            var menu = $('.header__menu').offset().top;
            $(window).scroll(function() {
                if ($(this).scrollTop() > menu) {
                    $('.header__menu').addClass('is-fixed');
                } else {
                    $('.header__menu').removeClass('is-fixed');
                }
            });
        }
    });

   /*
   |--------------------------------------------------------------------------
   | Showing modal with effect
   |--------------------------------------------------------------------------
   */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

    /*
    |--------------------------------------------------------------------------
    | Mobile Menu
    |--------------------------------------------------------------------------
    */

    var burger = $('.js-menu-trigger');

    // Sidebar toggle to sidebar-folded
    burger.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        // Toggle Menu
        $('.m-menu').toggleClass('is-show');

        // Disable Scroll On Body
        if ($(this).hasClass('is-open')) {
            $('body').css({"overflow": "hidden"});
        } else {
            $('body').css({"overflow": ""});
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Responsive Iframe Inside Modal
    |--------------------------------------------------------------------------
    */

    function toggle_video_modal() {

        // Click on video thumbnail or link
        $(".js-video-modal").on("click", function(e){

            // prevent default behavior for a-tags, button tags, etc.
            e.preventDefault();

            // Grab the video ID from the element clicked
            var id = $(this).attr('data-youtube-id');

            // Autoplay when the modal appears
            // Note: this is intetnionally disabled on most mobile devices
            // If critical on mobile, then some alternate method is needed
            var autoplay = '?autoplay=1';

            // Don't show the 'Related Videos' view when the video ends
            var related_no = '&rel=0';

            // String the ID and param variables together
            var src = '//www.youtube.com/embed/'+id+autoplay+related_no;

            // Pass the YouTube video ID into the iframe template...
            // Set the source on the iframe to match the video ID
            $(".video-modal__iframe").attr('src', src);

            // Add class to the body to visually reveal the modal
            $("body").addClass("video-modal-show");

            $('body').css({"overflow": "hidden"});

        });

        // Close and Reset the Video Modal
        function close_video_modal() {

            event.preventDefault();

            // re-hide the video modal
            $("body").removeClass("video-modal-show");

            $('body').css({"overflow": ""});

            // reset the source attribute for the iframe template, kills the video
            $(".video-modal__iframe").attr('src', '');

        }
        // if the 'close' button/element, or the overlay are clicked
        $('body').on('click', '.video-modal__close, .video-modal__overlay', function(event) {

            // call the close and reset function
            close_video_modal();

        });
        // if the ESC key is tapped
        $('body').keyup(function(e) {
            // ESC key maps to keycode `27`
            if (e.keyCode == 27) {

                // call the close and reset function
                close_video_modal();

            }
        });
    }
    toggle_video_modal();

    /*
    |--------------------------------------------------------------------------
    | Entry Slider
    |--------------------------------------------------------------------------
    */

    var entrySlider = new Swiper('.js-entry-slider', {
        speed: 600,
        mousewheel: false,
        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: '.js-entry-slider-next',
            prevEl: '.js-entry-slider-prev',
        },
        touchRatio: 1,
        pagination: {
            el: '.js-entry-slider-pagination',
            clickable: true,
        },
        autoHeight: true,
        slidesPerView: 1,
    });

    /*
    |--------------------------------------------------------------------------
    | Plans Slider
    |--------------------------------------------------------------------------
    */

	var plansSlider = new Swiper('.js-plans-slider', {
		speed: 600,
		mousewheel: false,
		loop: false,
		spaceBetween: 30,
		navigation: {
			nextEl: '.js-plans-slider-next',
			prevEl: '.js-plans-slider-prev',
		},
        touchRatio: 0,
        pagination: {
            el: '.js-plans-slider-pagination',
            clickable: true,
        },
		slidesPerView: 4,
        autoHeight: false,
        thumbs: {
            swiper: {
                el: '.js-features-slider',
                spaceBetween: 0,
                slidesPerView: 1,
                speed: 700,
                touchRatio: 1,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            }
        },
        breakpoints: {
            1280: {
                slidesPerView: 4,
            },
            1200: {
                slidesPerView: 3,
                touchRatio: 1,
                autoHeight: false,
            },
            768: {
                slidesPerView: 2,
                touchRatio: 1,
                autoHeight: true,
            },
            640: {
                slidesPerView: 1,
                touchRatio: 1,
                autoHeight: true,
            },
            320: {
                slidesPerView: 1,
                touchRatio: 1,
                autoHeight: true,
            }
        }
	});

    /*
    |--------------------------------------------------------------------------
    | Plans Slider 2
    |--------------------------------------------------------------------------
    */

    var plansSliderB = new Swiper('.js-plans-slider2', {
        speed: 600,
        mousewheel: false,
        loop: false,
        spaceBetween: 30,
        navigation: {
            nextEl: '.js-office-slider2-next',
            prevEl: '.js-office-slider2-prev',
        },
        touchRatio: 0,
        pagination: {
            el: '.js-plans-slider2-pagination',
            clickable: true,
        },
        slidesPerView: 3,
        autoHeight: false,
        thumbs: {
            swiper: {
                el: '.js-features-slider',
                spaceBetween: 0,
                slidesPerView: 1,
                speed: 700,
                touchRatio: 1,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
            }
        },
        breakpoints: {
            1280: {
                slidesPerView: 3,
            },
            992: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
                touchRatio: 1,
                autoHeight: true,
            },
            640: {
                slidesPerView: 1,
                touchRatio: 1,
                autoHeight: true,
            },
            320: {
                slidesPerView: 1,
                touchRatio: 1,
                autoHeight: true,
            }
        }
    });


    /*
    |--------------------------------------------------------------------------
    | Partners Slider
    |--------------------------------------------------------------------------
    */

    let partnersSlider = new Swiper('.js-partners-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        /*autoplay: {
            delay: 1000,
            disableOnInteraction: false,
        },*/
        autoHeight: true,
        spaceBetween: 30,
        navigation: {
            nextEl: '.js-partners-slider-next',
            prevEl: '.js-partners-slider-prev',
        },
        pagination: {
            el: '.js-partners-slider-pagination',
            clickable: true,
        },
        slidesPerView: 4,
        breakpoints: {
            1024: {
                slidesPerView: 4,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
            },
            320: {
                slidesPerView: 2,
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Light Gallery
    |--------------------------------------------------------------------------
    */

	$('.js-lg').lightGallery({
		selector: ".js-lg-item",
	});


    /*
    |--------------------------------------------------------------------------
    | Bootstrap Tooltip
    |--------------------------------------------------------------------------
    */

    $('[data-toggle="tooltip"]').tooltip();
    // colored tooltip
    $('[data-toggle="tooltip-primary"]').tooltip({
        template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });
    $('[data-toggle="tooltip-secondary"]').tooltip({
        template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });

    /*
    |--------------------------------------------------------------------------
    | Back to Top
    |--------------------------------------------------------------------------
    */

    $(window).on("scroll", function(e) {
        if ($(this).scrollTop() > 0) {
            $('.js-back-to-top').fadeIn('slow');
        } else {
            $('.js-back-to-top').fadeOut('slow');
        }
    });

    $(".js-back-to-top").on("click", function(e) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });




    /*
    |--------------------------------------------------------------------------
    | Particle Js
    |--------------------------------------------------------------------------
    */

    jQuery(document).ready(function($) {
        $(".js-particles").each(function() {
            particlesJS($(this).attr('id'), {
                "particles": {
                    "number": {
                        "value": 80,
                        "density": {
                            "enable": true,
                            "value_area": 700
                        }
                    },
                    "color": {
                        "value": "#2074C2"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#000000"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 0.1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 3,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 10,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#2074C2",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 2,
                        "direction": "none",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "bounce": false,
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "grab"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 150,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200,
                            "duration": 0.4
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true
            });

        });
    });

});



